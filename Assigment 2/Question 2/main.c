#include <stdio.h>

int main() {
    int number;

    printf("\n\nEnter number: ");
    scanf("%d", &number);

    if (number % 2 == 0){
        printf("\nThe number is Even");
    } else{
        printf("\nThe number is Odd");
    }
    return 0;
}
