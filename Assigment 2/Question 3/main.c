#include <stdio.h>
#include <ctype.h>

int main() {
    char letter;

    printf("Enter a letter");
    scanf("%s", &letter);

    letter = tolower(letter);

    if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
        printf("This letter is a vowel");
    } else {
        printf("This letter is a constant");
    }
    return 0;
}
