#include <stdio.h>

int main() {
    int number;

    printf("\n\nEnter number: ");
    scanf("%d", &number);

    if (number > 0 || number == 0){
        printf("\nThe number is positive");
    } else{
        printf("\nThe number is negative");
    }
    return 0;
}
