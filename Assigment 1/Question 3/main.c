#include <stdio.h>


int main() {
    int firstNumber;
    int secondNumber;
    char firstNumberStr[50] ;
    char secondNumberStr[50];

    printf("\n\nEnter the first number: ");
    scanf("%d", &firstNumber);

    printf("\n\nEnter the second number: ");
    scanf("%d", &secondNumber);

    sprintf(firstNumberStr, "%f", firstNumber);
    sprintf(secondNumberStr, "%f", secondNumber);

    sprintf(firstNumberStr, "%d", firstNumber);
    sprintf(secondNumberStr, "%d", secondNumber);

    printf("\n Before swapping %s", firstNumberStr);
    printf("%s", secondNumberStr);


    printf("\n\n After swapping %s", secondNumberStr);

    printf("%s", firstNumberStr);


    return 0;
}
