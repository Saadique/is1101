#include <stdio.h>
#include "math.h"

int main() {
    double radius;
    double pie = 22/7;

    printf("\n\nEnter the radius: ");
    scanf("%lf", &radius);

    printf("\n The area of the circle is : %lf", pow(radius,2) * pie);
}
