#include <stdio.h>

int main() {

    float firstNumber;
    float secondNumber;

    // Input the float
    printf("\nEnter the first Number: ");
    scanf("%f", &firstNumber);

    printf("\nEnter the second Number: ");
    scanf("%f", &secondNumber);

    // Output the float
    printf("\nEntered first number is : %f", firstNumber);
    printf("\nEntered second number is: %f", secondNumber);

    printf("\n\nMultiplication of two numbers is : %f", firstNumber * secondNumber);


    return 0;
}


