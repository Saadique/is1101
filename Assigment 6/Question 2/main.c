#include <stdio.h>

int fibonacciSeq(int);


int main()
{
    int limit;

    printf("Enter limit: ");
    scanf("%d", &limit);

    for(int n = 0; n <= limit; n++)
    {
        printf("%d ", fibonacciSeq(n));
    }

    return 0;
}


int fibonacciSeq(int n)
{
    if(n == 0 || n == 1)
    {
        return n;
    }

    else
    {
        return fibonacciSeq(n - 1) + fibonacciSeq(n - 2);
    }

}
