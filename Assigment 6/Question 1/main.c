#include <stdio.h>

void printn(int num)
{
    if (num == 0)
        return;
    printf("%d", num) ;
    printn(num - 1);
}

void pattern(int n, int i)
{
    if (n == 0)
        return;
    printn(i);
    printf("\n");

    pattern(n - 1, i + 1);
}

int main()
{
    int n = 4;
    pattern(n, 1);
    return 0;
}
