#include <stdio.h>
#include<string.h>

struct student {
    char name[50];
    char sub[25];
    int mark;
};

struct student s[5];

int main() {
    int i;
    for(i=0;i<5;i++){
        printf("Enter student %d name:",i+1);
        scanf(" %[^\n]s",s[i].name);

        printf("Enter subject:");
        scanf(" %[^\n]s",s[i].sub);

        printf("Enter mark :");
        scanf("%d",&s[i].mark);

    }

    for(i=0;i<5;i++){
        printf("\n");
        printf("Student %d\n ", i+1);
        printf("Student Name    - %s\n",s[i].name);
        printf("Student Subject -  %s\n",s[i].sub);
        printf("Student Marks   - %d\n",s[i].mark);
        printf("\n");
    }
}
